package com.serkangurel.yemektarifleri.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.serkangurel.yemektarifleri.fragments.RecipesFragment;

public class CategoryActivity extends BaseActivity {

    private RecipesFragment recipesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Intent intent = getIntent();
        String category = intent.getStringExtra(Constans.INTENT_KEY);

        recipesFragment = RecipesFragment.newInstance(category, "");
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.category_container, recipesFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}