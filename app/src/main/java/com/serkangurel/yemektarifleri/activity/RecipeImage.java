package com.serkangurel.yemektarifleri.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

public class RecipeImage extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_image);

        TextView tvTitle = findViewById(R.id.tv_title);
        ImageView imageBack = findViewById(R.id.img_back);
        PhotoView photoView = findViewById(R.id.photo_view);

        Intent intent = getIntent();
        String url = intent.getStringExtra(Constans.INTENT_KEY);
        String title = intent.getStringExtra(Constans.INTENT_KEY2);

        imageBack.setOnClickListener(view -> onBackPressed());
        tvTitle.setText(title);

        if (!TextUtils.isEmpty(url)) {
            Glide.with(this).load(url).into(photoView);
        }

    }
}
