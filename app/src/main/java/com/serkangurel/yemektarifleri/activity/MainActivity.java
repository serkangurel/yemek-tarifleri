package com.serkangurel.yemektarifleri.activity;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.orhanobut.hawk.Hawk;
import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.adapter.RecipesPagerAdapter;
import com.serkangurel.yemektarifleri.customviews.NonSwipeableViewPager;
import com.serkangurel.yemektarifleri.fragments.RecipesFragment;
import com.serkangurel.yemektarifleri.utils.Constans;

public class MainActivity extends BaseActivity {

    private NonSwipeableViewPager viewPager;
    private boolean isRecipesFragment = true;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String ad_interstitial = Hawk.get(Constans.FIREBASE_INTERSTITIAL, Constans.ADMOB_INTERSTITIAL_KEY);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(ad_interstitial);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        RecipesPagerAdapter pagerAdapter = new RecipesPagerAdapter(getSupportFragmentManager());
        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2);

        BottomNavigationView navigation = findViewById(R.id.navigation_bottom);
        navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_recipes:
                    viewPager.setCurrentItem(0);
                    if (isRecipesFragment) {
                        RecipesFragment recipesFragment = (RecipesFragment) pagerAdapter.getCurrentFragment(viewPager);
                        if (recipesFragment != null) {
                            RecyclerView recyclerView = recipesFragment.getRecyclerView();
                            recyclerView.smoothScrollToPosition(0);
                        }
                    }
                    isRecipesFragment = true;
                    return true;
                case R.id.navigation_categories:
                    viewPager.setCurrentItem(1);
                    isRecipesFragment = false;
                    return true;
                case R.id.navigation_favorites:
                    viewPager.setCurrentItem(2);
                    isRecipesFragment = false;
                    return true;
            }
            return false;
        });
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        super.onBackPressed();
    }

}