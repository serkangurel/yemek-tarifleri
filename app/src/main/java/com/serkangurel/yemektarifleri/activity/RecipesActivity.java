package com.serkangurel.yemektarifleri.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.rxdownloader.RxDownloader;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.orhanobut.hawk.Hawk;
import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.db.RecipesEntity;
import com.serkangurel.yemektarifleri.otto.BusProvider;
import com.serkangurel.yemektarifleri.otto.UpdateFavorites;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.serkangurel.yemektarifleri.utils.Tools;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.ArrayList;
import java.util.List;

public class RecipesActivity extends BaseActivity {

    private static final String TAG = "RecipesActivity";
    private String yemekAdi;
    private int adIndex = 1;
    private InterstitialAd mInterstitialAd;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);

        LinearLayout linearLayout = findViewById(R.id.ll_recipes);

        ImageView imageRecipes = findViewById(R.id.image_recipes);
        ImageView imageBack = findViewById(R.id.img_back);
        ImageView imageShare = findViewById(R.id.img_share);

        TextView tvRecipesTime = findViewById(R.id.tv_recipes_time);
        TextView tvTitle = findViewById(R.id.tv_title);
        TextView tvRecipesName = findViewById(R.id.tv_recipes_name);
        TextView tvRecipesPeople = findViewById(R.id.tv_recipes_people);
        TextView tvIngredients = findViewById(R.id.tv_ingredients);
        TextView tvInstruction = findViewById(R.id.tv_instruction);

        LikeButton btn_heart = findViewById(R.id.btn_heart);

        String ad_banner = Hawk.get(Constans.FIREBASE_BANNER, Constans.ADMOB_BANNER_KEY);
        String ad_interstital = Hawk.get(Constans.FIREBASE_INTERSTITIAL, Constans.ADMOB_INTERSTITIAL_KEY);

        AdView mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(ad_banner);
        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                        , ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        params.bottomMargin = 6;
        params.topMargin = 6;
        linearLayout.addView(mAdView, adIndex, params);
        mAdView.loadAd(new AdRequest.Builder().build());

        boolean isInterstitalShown = Hawk.get(Constans.HAWK_IS_INTESTITIAL_SHOWN, false);
        if (!isInterstitalShown) {
            displayInterstital(ad_interstital);
        }

        Intent intent = getIntent();
        RecipesEntity recipe = (RecipesEntity) intent.getSerializableExtra(Constans.INTENT_KEY);

        yemekAdi = Tools.getInstance().firstLetterCap(recipe.yemekAdi);

        tvTitle.setText(yemekAdi);
        tvRecipesName.setText(yemekAdi);
        tvRecipesTime.setText(recipe.hazirlamaSuresi + " " + getString(R.string.time));
        tvRecipesPeople.setText(recipe.kisiSayisi + " " + getString(R.string.people));

        tvIngredients.setText(recipe.malzemeler);
        tvInstruction.setText(recipe.yemekTarifi);

        List<RecipesEntity> savedList = Hawk.get(Constans.HAWK_FAVORITES, new ArrayList<>());

        for (RecipesEntity recipesEntity : savedList) {
            if (recipesEntity.id == recipe.id) {
                btn_heart.setLiked(true);
            }
        }

        String url = "https://firebasestorage.googleapis.com/v0/b/apps-44007.appspot.com/o/"
                + recipe.resim + ".jpg?alt=media";
        Glide.with(RecipesActivity.this).load(url).into(imageRecipes);

        imageBack.setOnClickListener(view -> onBackPressed());

        imageShare.setOnClickListener(view -> {
            RxPermissions rxPermissions = new RxPermissions(RecipesActivity.this);
            rxPermissions
                    .request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                    .subscribe(granted -> {
                        if (granted) {
                            Toast.makeText(this, "Paylaşılıyor...", Toast.LENGTH_LONG).show();
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                            request.setMimeType("image/jpg");
                            request.setDestinationInExternalPublicDir(Tools.getInstance().getDownloadPath(), yemekAdi + ".jpg");
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);

                            RxDownloader.getInstance(RecipesActivity.this)
                                    .download(request)
                                    .subscribe(path -> {
                                        Intent shareIntent = new Intent();
                                        shareIntent.setAction(Intent.ACTION_SEND);
                                        shareIntent.putExtra(Intent.EXTRA_TEXT,
                                                yemekAdi + "\n\nMalzemeler\n\n"
                                                        + recipe.malzemeler + "\n\nYapılışı\n\n"
                                                        + recipe.yemekTarifi);
                                        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                                        shareIntent.setType("image/jpg");
                                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                        startActivity(Intent.createChooser(shareIntent, getString(R.string.send)));

                                    });
                        }

                    });
        });

        imageRecipes.setOnClickListener(view -> {
            Intent myIntent = new Intent(RecipesActivity.this, RecipeImage.class);
            myIntent.putExtra(Constans.INTENT_KEY, url);
            myIntent.putExtra(Constans.INTENT_KEY2, yemekAdi);
            startActivity(myIntent);
        });

        btn_heart.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                List<RecipesEntity> favorites = Hawk.get(Constans.HAWK_FAVORITES, new ArrayList<>());
                favorites.add(recipe);
                Hawk.put(Constans.HAWK_FAVORITES, favorites);
                BusProvider.getInstance().post(new UpdateFavorites());
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                List<RecipesEntity> favorites = Hawk.get(Constans.HAWK_FAVORITES, new ArrayList<>());
                for (int i = 0; i < favorites.size(); i++) {
                    if (favorites.get(i).id == recipe.id) {
                        favorites.remove(i);
                    }
                }
                favorites.remove(recipe);
                Hawk.put(Constans.HAWK_FAVORITES, favorites);
                BusProvider.getInstance().post(new UpdateFavorites());
            }
        });
    }

    private void displayInterstital(String ad_interstital) {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(ad_interstital);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mInterstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.e(TAG, "onAdFailedToLoad: " + errorCode);
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                Hawk.put(Constans.HAWK_IS_INTESTITIAL_SHOWN, true);
            }
        });
    }

}