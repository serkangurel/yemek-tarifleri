package com.serkangurel.yemektarifleri;

import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.orhanobut.hawk.Hawk;
import com.serkangurel.yemektarifleri.utils.Constans;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class YTApp extends MultiDexApplication {
    public static Resources RESOURCES;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    public void onCreate() {
        super.onCreate();
        RESOURCES = getResources();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        Fabric.with(this, new Crashlytics());
        MobileAds.initialize(this, Constans.ADMOB_APP_ID);
        Hawk.init(this).build();
        setRemoteConfigData();
        Hawk.put(Constans.HAWK_IS_INTESTITIAL_SHOWN, false);
    }

    private void setRemoteConfigData() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        long cacheExpiration = 3600; // 1 hour in seconds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String ad_interstitial = mFirebaseRemoteConfig.getString(Constans.FIREBASE_INTERSTITIAL);
                        String ad_banner = mFirebaseRemoteConfig.getString(Constans.FIREBASE_BANNER);

                        String interstitial = !TextUtils.isEmpty(ad_interstitial) ? ad_interstitial : Constans.ADMOB_INTERSTITIAL_KEY;
                        Hawk.put(Constans.FIREBASE_INTERSTITIAL, interstitial);

                        String banner = !TextUtils.isEmpty(ad_banner) ? ad_banner : Constans.ADMOB_BANNER_KEY;
                        Hawk.put(Constans.FIREBASE_BANNER, banner);

                        mFirebaseRemoteConfig.activateFetched();
                    }
                });
    }
}