package com.serkangurel.yemektarifleri.model;

import java.util.List;

public class Recipes {

    public List<YemeklerBean> yemekler;

    public static class YemeklerBean {
        public int hazirlamaSuresi;
        public int id;
        public String kategori;
        public int kisiSayisi;
        public String malzemeler;
        public String resim;
        public int sure;
        public String yemekAdi;
        public String yemekTarifi;


    }
}
