package com.serkangurel.yemektarifleri.model;

import java.util.List;

public class Category {

    public List<CategoryBean> category;

    public static class CategoryBean {
        public String categoryName;
        public String image;
    }
}