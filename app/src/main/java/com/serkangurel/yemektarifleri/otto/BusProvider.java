package com.serkangurel.yemektarifleri.otto;

import com.squareup.otto.Bus;

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }

//	class AsyncBus extends Bus {
//
//		private final Handler mainThread = new Handler(Looper.getMainLooper());
//
//		@Override
//		public void post(final Object event) {
//			mainThread.post(new Runnable() {
//				@Override
//				public void run() {
//					AsyncBus.super.post(event);
//				}
//			});
//		}
//	}

}
