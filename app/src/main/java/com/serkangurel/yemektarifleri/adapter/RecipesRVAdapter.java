package com.serkangurel.yemektarifleri.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.activity.RecipesActivity;
import com.serkangurel.yemektarifleri.db.RecipesEntity;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.serkangurel.yemektarifleri.utils.Tools;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class RecipesRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<RecipesEntity> recipesList = new ArrayList<>();
    private List<RecipesEntity> sliderList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private String category = "";
    private String searchedText = "";

    private enum Type {
        SLIDER, RECIPES, ADS
    }

    public RecipesRVAdapter(Context context, String category) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.category = category;
    }

    public void initLists(List<RecipesEntity> recipesList, List<RecipesEntity> sliderList, String searchedText) {
        this.sliderList = sliderList;
        setList(recipesList, searchedText);
    }

    public void setList(List<RecipesEntity> recipesList, String searchedText) {
        this.searchedText = searchedText;
        this.recipesList.clear();
        this.recipesList.addAll(recipesList);
        notifyDataSetChanged();
    }

    public List<RecipesEntity> getList() {
        return recipesList;
    }

    public void addList(List<RecipesEntity> list) {
        if (list != null) {
            final int pos = getItemCount();
            recipesList.addAll(list);
            notifyItemRangeInserted(pos, list.size());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && TextUtils.isEmpty(category) && TextUtils.isEmpty(searchedText))
            return Type.SLIDER.ordinal();
        else if (position > 0 && position % 6 == 0)
            return Type.ADS.ordinal();
        else return Type.RECIPES.ordinal();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Type.SLIDER.ordinal()) {
            View view = mInflater.inflate(R.layout.slideshow_vpa, parent, false);
            return new SliderViewHolder(view);
        } else if (viewType == Type.ADS.ordinal()) {
            View view = mInflater.inflate(R.layout.item_ads, parent, false);
            return new AdsViewHolder(view);
        } else {
            View view = mInflater.inflate(R.layout.item_recipes, parent, false);
            return new RecipesViewHolder(view);
        }
    }

    @SuppressLint({"CheckResult", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SliderViewHolder) {
            SliderViewHolder sliderHolder = (SliderViewHolder) holder;

            SliderPagerAdapter mPageAdapter = new SliderPagerAdapter(context);
            mPageAdapter.setList(sliderList);
            sliderHolder.viewPager.setAdapter(mPageAdapter);
            sliderHolder.indicator.setViewPager(sliderHolder.viewPager);
            sliderHolder.previousPage.setOnClickListener(view ->
                    sliderHolder.viewPager.setCurrentItem(sliderHolder.viewPager.getCurrentItem() - 1, true));
            sliderHolder.nextPage.setOnClickListener(view ->
                    sliderHolder.viewPager.setCurrentItem(sliderHolder.viewPager.getCurrentItem() + 1, true));

        } else if (holder instanceof AdsViewHolder) {
            AdsViewHolder adsViewHolder = (AdsViewHolder) holder;

            if (adsViewHolder.linearLayout.getChildCount() == 0) {
                String ad_banner = Hawk.get(Constans.FIREBASE_BANNER);
                AdView mAdView = new AdView(context);
                mAdView.setAdSize(AdSize.LARGE_BANNER);
                mAdView.setAdUnitId(ad_banner);
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                                , ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                adsViewHolder.linearLayout.addView(mAdView, 0, params);

                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            } else {
                AdView mAdView = (AdView) adsViewHolder.linearLayout.getChildAt(0);

                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }
        } else {
            RecipesViewHolder recipesHolder = (RecipesViewHolder) holder;
            //https://firebasestorage.googleapis.com/v0/b/apps-44007.appspot.com/o/acibademkurabiyesi.jpg?alt=media
            String url = "https://firebasestorage.googleapis.com/v0/b/apps-44007.appspot.com/o/"
                    + recipesList.get(position).resim + ".jpg?alt=media";

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(context.getResources().getDrawable(R.drawable.placeholder));

            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(url)
                    .into(recipesHolder.image);

            recipesHolder.title.setText(Tools.getInstance().firstLetterCap(recipesList.get(position).yemekAdi));
            recipesHolder.time.setText(recipesList.get(position).hazirlamaSuresi + " " + context.getString(R.string.time));
            recipesHolder.people.setText(recipesList.get(position).kisiSayisi + " " + context.getString(R.string.people));
            recipesHolder.itemView.setOnClickListener(view -> {
                Intent myIntent = new Intent(context, RecipesActivity.class);
                myIntent.putExtra(Constans.INTENT_KEY, recipesList.get(position));
                context.startActivity(myIntent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return recipesList.size();
    }

    public class RecipesViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        TextView time;
        TextView people;

        public RecipesViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.iv_image);
            title = itemView.findViewById(R.id.tv_ingredients);
            time = itemView.findViewById(R.id.tv_time);
            people = itemView.findViewById(R.id.tv_people);
        }
    }

    public class SliderViewHolder extends RecyclerView.ViewHolder {

        ViewPager viewPager;
        CircleIndicator indicator;
        ImageView nextPage;
        ImageView previousPage;

        public SliderViewHolder(View itemView) {
            super(itemView);
            viewPager = itemView.findViewById(R.id.viewpager_slideshow);
            indicator = itemView.findViewById(R.id.indicator);
            nextPage = itemView.findViewById(R.id.next_page);
            previousPage = itemView.findViewById(R.id.previous_page);
        }
    }

    public class AdsViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;

        public AdsViewHolder(View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.ll_recipes);
        }
    }
}
