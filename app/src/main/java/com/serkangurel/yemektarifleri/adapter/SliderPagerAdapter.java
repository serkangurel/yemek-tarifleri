package com.serkangurel.yemektarifleri.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.activity.RecipesActivity;
import com.serkangurel.yemektarifleri.db.RecipesEntity;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.serkangurel.yemektarifleri.utils.Tools;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class SliderPagerAdapter extends PagerAdapter {

    private Context context;
    private final List<RecipesEntity> recipesList = new ArrayList<>();

    public SliderPagerAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<RecipesEntity> recipesList) {
        this.recipesList.clear();
        this.recipesList.addAll(recipesList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return recipesList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_slideshow, container, false);
        ImageView imageRecipes = itemView.findViewById(R.id.img_recipes);
        TextView title = itemView.findViewById(R.id.txt_recipes_title);
        imageRecipes.setOnClickListener(view -> {
            Intent myIntent = new Intent(context, RecipesActivity.class);
            myIntent.putExtra(Constans.INTENT_KEY, recipesList.get(position));
            context.startActivity(myIntent);
        });

        String url = "https://firebasestorage.googleapis.com/v0/b/apps-44007.appspot.com/o/"
                + recipesList.get(position).resim + ".jpg?alt=media";
        Glide.with(context).load(url).into(imageRecipes);
        title.setText(Tools.getInstance().firstLetterCap(recipesList.get(position).yemekAdi));

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}