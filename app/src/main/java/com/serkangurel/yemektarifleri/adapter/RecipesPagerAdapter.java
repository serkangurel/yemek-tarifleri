package com.serkangurel.yemektarifleri.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.fragments.CategoriesFragment;
import com.serkangurel.yemektarifleri.fragments.FavoritesFragment;
import com.serkangurel.yemektarifleri.fragments.RecipesFragment;

public class RecipesPagerAdapter extends FragmentPagerAdapter {

    private FragmentManager fm;

    public RecipesPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return RecipesFragment.newInstance("", "");
            case 1:
                return CategoriesFragment.newInstance("", "");
            case 2:
                return FavoritesFragment.newInstance("", "");
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public Fragment getFragment(int pos) {
        return fm.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + pos);
    }

    public Fragment getCurrentFragment(ViewPager viewPager) {
        return getFragment(viewPager.getCurrentItem());
    }
}
