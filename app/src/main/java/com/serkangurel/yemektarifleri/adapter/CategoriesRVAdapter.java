package com.serkangurel.yemektarifleri.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.activity.CategoryActivity;
import com.serkangurel.yemektarifleri.model.Category.CategoryBean;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class CategoriesRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<CategoryBean> categoryList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public CategoriesRVAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setList(List<CategoryBean> categoryList) {
        this.categoryList.clear();
        this.categoryList.addAll(categoryList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_category, parent, false);
        return new CategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CategoriesViewHolder categoryHolder = (CategoriesViewHolder) holder;
        String url = "https://firebasestorage.googleapis.com/v0/b/apps-44007.appspot.com/o/"
                + categoryList.get(position).image + ".jpg?alt=media";
        Glide.with(context).load(url).into(categoryHolder.imageView);

        categoryHolder.categoryName.setText(categoryList.get(position).categoryName);

        categoryHolder.itemView.setOnClickListener(view -> {
            Intent myIntent = new Intent(context, CategoryActivity.class);
            myIntent.putExtra(Constans.INTENT_KEY, categoryList.get(position).categoryName);
            context.startActivity(myIntent);
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView categoryName;

        public CategoriesViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.circleImage);
            categoryName = itemView.findViewById(R.id.categoryName);
        }
    }

}
