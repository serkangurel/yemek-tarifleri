package com.serkangurel.yemektarifleri.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.serkangurel.yemektarifleri.activity.RecipesActivity;
import com.serkangurel.yemektarifleri.db.RecipesEntity;
import com.serkangurel.yemektarifleri.utils.Tools;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class FavoritesRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<RecipesEntity> recipesList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public FavoritesRVAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setList(List<RecipesEntity> recipesList) {
        this.recipesList.clear();
        this.recipesList.addAll(recipesList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_recipes, parent, false);
        return new RecipesViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RecipesViewHolder recipesHolder = (RecipesViewHolder) holder;
        String url = "https://firebasestorage.googleapis.com/v0/b/apps-44007.appspot.com/o/"
                + recipesList.get(position).resim + ".jpg?alt=media";
        Glide.with(context).load(url).into(recipesHolder.image);
        recipesHolder.title.setText(Tools.getInstance().firstLetterCap(recipesList.get(position).yemekAdi));
        recipesHolder.time.setText(recipesList.get(position).hazirlamaSuresi + " " + context.getString(R.string.time));
        recipesHolder.people.setText(recipesList.get(position).kisiSayisi + " " + context.getString(R.string.people));
        recipesHolder.itemView.setOnClickListener(view -> {
            Intent myIntent = new Intent(context, RecipesActivity.class);
            myIntent.putExtra(Constans.INTENT_KEY, recipesList.get(position));
            context.startActivity(myIntent);
        });
    }

    @Override
    public int getItemCount() {
        return recipesList.size();
    }

    public class RecipesViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        TextView time;
        TextView people;

        public RecipesViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.iv_image);
            title = itemView.findViewById(R.id.tv_ingredients);
            time = itemView.findViewById(R.id.tv_time);
            people = itemView.findViewById(R.id.tv_people);
        }
    }
}