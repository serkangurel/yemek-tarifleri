package com.serkangurel.yemektarifleri.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.adapter.FavoritesRVAdapter;
import com.serkangurel.yemektarifleri.db.RecipesEntity;
import com.serkangurel.yemektarifleri.otto.UpdateFavorites;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.orhanobut.hawk.Hawk;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends BaseFragment {

    private RelativeLayout layoutNoFavorites;
    private FrameLayout layoutFavorites;
    private FavoritesRVAdapter adapter;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    public static FavoritesFragment newInstance(String param1, String param2) {
        FavoritesFragment fragment = new FavoritesFragment();
        Bundle args = new Bundle();
        args.putString(Constans.ARG_PARAM1, param1);
        args.putString(Constans.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        RecyclerView rvFavorites = view.findViewById(R.id.rvFavorites);
        layoutNoFavorites = view.findViewById(R.id.no_favorites);
        layoutFavorites = view.findViewById(R.id.favorites);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            activity.setSupportActionBar(toolbar);
        }

        adapter = new FavoritesRVAdapter(getContext());

        List<RecipesEntity> savedList = Hawk.get(Constans.HAWK_FAVORITES, new ArrayList<>());
        updateUI(savedList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFavorites.setLayoutManager(layoutManager);

        rvFavorites.setAdapter(adapter);
        adapter.setList(savedList);
    }

    @Subscribe
    public void updateFavorites(UpdateFavorites event) {
        List<RecipesEntity> favorites = Hawk.get(Constans.HAWK_FAVORITES, new ArrayList<>());
        adapter.setList(favorites);
        updateUI(favorites);
    }

    private void updateUI(List<RecipesEntity> favoritesList) {
        if (favoritesList.size() > 0) {
            layoutNoFavorites.setVisibility(View.GONE);
            layoutFavorites.setVisibility(View.VISIBLE);
        } else {
            layoutNoFavorites.setVisibility(View.VISIBLE);
            layoutFavorites.setVisibility(View.GONE);
        }
    }
}