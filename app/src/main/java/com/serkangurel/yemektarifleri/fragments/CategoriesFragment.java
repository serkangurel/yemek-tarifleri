package com.serkangurel.yemektarifleri.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.adapter.CategoriesRVAdapter;
import com.serkangurel.yemektarifleri.model.Category.CategoryBean;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.serkangurel.yemektarifleri.utils.Tools;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends BaseFragment {

    private List<CategoryBean> categoryList = new ArrayList<>();

    public CategoriesFragment() {
        // Required empty public constructor
    }

    public static CategoriesFragment newInstance(String param1, String param2) {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        args.putString(Constans.ARG_PARAM1, param1);
        args.putString(Constans.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    CategoriesRVAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(true);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView rvCategories = view.findViewById(R.id.rvCategories);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCategories.setLayoutManager(layoutManager);

        adapter = new CategoriesRVAdapter(getActivity());
        rvCategories.setAdapter(adapter);

//        AppDatabase db = AppDatabase.getAppDatabase(getActivity());
//        updateDB(db);
//        List<RecipesEntity> categoryList = db.recipesDao().getRecipesGroupByKategori();

        String json = Tools.getInstance().loadJSONFromAsset(getContext(), "categories.json");
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("category");

            Type listType = new TypeToken<List<CategoryBean>>() {
            }.getType();

            categoryList = new Gson().fromJson(jsonArray.toString(), listType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter.setList(categoryList);
    }

//    private void updateDB(AppDatabase db) {
//        for (int i = 0; i < 8; i++) {
//            RecipesEntity recipesEntity = db.recipesDao().getRecipeForUpdate();
//            if (recipesEntity == null) {
//                return;
//            } else {
//                recipesEntity.kategori = "Tavuklar";
//                db.recipesDao().updateRecipes(recipesEntity);
//            }
//        }
//    }
}
