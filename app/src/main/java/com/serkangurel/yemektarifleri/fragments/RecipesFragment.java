package com.serkangurel.yemektarifleri.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.serkangurel.yemektarifleri.R;
import com.serkangurel.yemektarifleri.adapter.RecipesRVAdapter;
import com.serkangurel.yemektarifleri.db.AppDatabase;
import com.serkangurel.yemektarifleri.db.RecipesEntity;
import com.serkangurel.yemektarifleri.utils.Constans;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */

public class RecipesFragment extends BaseFragment {

    private String category = "";

    public static final int LIMIT = 20;

    private RecyclerView rvRecipes;

    private int index = 0;
    private String searchedText = "";

    private RecipesRVAdapter adapter;

    private Disposable currentSearch;

    private AppDatabase db;
    private boolean hasLoadedAllItems = false;

    public ImageView imageBack;

    public RecipesFragment() {
        // Required empty public constructor
    }

    public static RecipesFragment newInstance(String param1, String param2) {
        RecipesFragment fragment = new RecipesFragment();
        Bundle args = new Bundle();
        args.putString(Constans.ARG_PARAM1, param1);
        args.putString(Constans.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recipes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SearchView searchView = view.findViewById(R.id.search_view_main);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        rvRecipes = view.findViewById(R.id.rvRecipes);
        TextView title = view.findViewById(R.id.recipes_title);

        imageBack = view.findViewById(R.id.img_back);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.getString(Constans.ARG_PARAM1).length() > 0) {
            category = bundle.getString(Constans.ARG_PARAM1);
            title.setText(category);
            imageBack.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.leftMargin = 0;
                params.addRule(RelativeLayout.CENTER_VERTICAL);
                params.addRule(RelativeLayout.RIGHT_OF, R.id.img_back);
                params.addRule(RelativeLayout.END_OF, R.id.img_back);
                title.setLayoutParams(params);
            }

        }

        imageBack.setOnClickListener(view1 -> {
            Objects.requireNonNull(getActivity()).onBackPressed();
        });

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            activity.setSupportActionBar(toolbar);
        }

        searchView.setOnQueryTextListener(queryTextListener);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        EditText editText = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        editText.setHintTextColor(getResources().getColor(android.R.color.black));
        editText.setTextColor(getResources().getColor(android.R.color.black));

        db = AppDatabase.getAppDatabase(getActivity());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvRecipes.setLayoutManager(layoutManager);

        adapter = new RecipesRVAdapter(getContext(), category);
        rvRecipes.setAdapter(adapter);

        Paginate.with(rvRecipes, callbacks)
                .setLoadingTriggerThreshold(5)
                .addLoadingListItem(true)
                .build();

        loadRecipesList(searchedText);

    }

    public RecyclerView getRecyclerView() {
        return rvRecipes;
    }

    private void loadRecipesList(String keyword) {

        if (currentSearch != null && !currentSearch.isDisposed()) {
            currentSearch.dispose();
        }

        currentSearch = Single.just(keyword)
                .subscribeOn(Schedulers.io())
                .delaySubscription(400, TimeUnit.MILLISECONDS)
                .map(s -> {
                    boolean isCategorySearch = category.length() > 0;

                    if (TextUtils.isEmpty(s)) {
                        if (isCategorySearch) { // Kategori
                            return db.recipesDao().getRecipesBySizeWithCategory(category, index, LIMIT);
                        }
                        List<Integer> intList = new ArrayList<>();
                        for (RecipesEntity recipesEntity : adapter.getList()) {
                            intList.add(recipesEntity.id);
                        }
                        return db.recipesDao().getRecipesBySize(index, LIMIT, intList); // Anasayfa
                    }
                    if (isCategorySearch) {
                        return db.recipesDao().getRecipesByQueryWithCategory(category, index, LIMIT, s); //Kategori arama
                    }
                    return db.recipesDao().getRecipesByQuery(index, LIMIT, s); //Anasayfa arama
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recipesEntities -> {
                    if (index > 0) {
                        adapter.addList(recipesEntities);
                    } else {
                        if (TextUtils.isEmpty(searchedText) && TextUtils.isEmpty(category)) {
                            int size = Math.min(10, recipesEntities.size());
                            adapter.initLists(recipesEntities.subList(size, recipesEntities.size()), recipesEntities.subList(0, size), searchedText);
                        } else {
                            adapter.setList(recipesEntities, searchedText);
                        }
                    }
                    index += index > 0 ? LIMIT : LIMIT / 2;
                    if (adapter.getItemCount() < index) {
                        hasLoadedAllItems = true;
                        Log.d("RecipesFragment", "hasLoadedAllItems = true");
                    }
                    Log.d("RecipesFragment", "index : " + index);
                    Log.d("RecipesFragment", "adapter.getItemCount() : " + adapter.getItemCount());
                });
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String newText) {
            searchedText = newText;
            index = 0;
            hasLoadedAllItems = false;
            loadRecipesList(searchedText);
            return true;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            return true;
        }
    };

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            loadRecipesList(searchedText);
        }

        @Override
        public boolean isLoading() {
            return false;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return hasLoadedAllItems;
        }
    };

}


