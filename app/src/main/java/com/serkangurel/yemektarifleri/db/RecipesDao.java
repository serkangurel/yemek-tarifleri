package com.serkangurel.yemektarifleri.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface RecipesDao {

    @Insert(onConflict = REPLACE)
    void insertRecipes(RecipesEntity... recipes);

    @Insert(onConflict = REPLACE)
    void insertRecipesByList(List<RecipesEntity> recipes);

    @Update(onConflict = REPLACE)
    void updateRecipes(RecipesEntity... recipes);

    @Update(onConflict = REPLACE)
    void updateRecipesByList(List<RecipesEntity> recipes);

    @Delete
    void deleteAll(List<RecipesEntity> recipes);

    @Query("SELECT * FROM Yemektarifleri")
    public List<RecipesEntity> getAllRecipes();

    @Query("SELECT * FROM Yemektarifleri WHERE kategori = 'Tavuklar '")
    public RecipesEntity getRecipeForUpdate();

    @Query("SELECT DISTINCT kategori FROM Yemektarifleri ORDER BY kategori ASC")
    public List<String> getAllCategories();

    @Query("SELECT * FROM Yemektarifleri ORDER BY RANDOM()")
    public List<RecipesEntity> shuffleAllRows();

    @Query("SELECT * FROM Yemektarifleri GROUP BY kategori")
    public List<RecipesEntity> getRecipesGroupByKategori();

    @Query("SELECT * FROM Yemektarifleri WHERE id NOT IN (:list) ORDER BY RANDOM() LIMIT :startNum, :limitNum")
    public List<RecipesEntity> getRecipesBySize(int startNum, int limitNum, List<Integer> list);

    @Query("SELECT * FROM Yemektarifleri WHERE yemekad LIKE '%' || :query || '%' LIMIT :startNum, :limitNum")
    public List<RecipesEntity> getRecipesByQuery(int startNum, int limitNum, String query);

    @Query("SELECT * FROM Yemektarifleri WHERE kategori = :category LIMIT :startNum, :limitNum")
    public List<RecipesEntity> getRecipesBySizeWithCategory(String category, int startNum, int limitNum);

    @Query("SELECT * FROM Yemektarifleri WHERE kategori = :category AND yemekad LIKE '%' || :query || '%' LIMIT :startNum, :limitNum")
    public List<RecipesEntity> getRecipesByQueryWithCategory(String category, int startNum, int limitNum, String query);


}
