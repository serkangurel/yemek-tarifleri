package com.serkangurel.yemektarifleri.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Yemektarifleri")
public class RecipesEntity implements Serializable {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "kategori")
    public String kategori;

    @ColumnInfo(name = "yemekad")
    public String yemekAdi;

    @ColumnInfo(name = "resim")
    public String resim;

    @ColumnInfo(name = "yemektarifi")
    public String yemekTarifi;

    @ColumnInfo(name = "hazirlamasuresi")
    public int hazirlamaSuresi;

    @ColumnInfo(name = "sure")
    public int sure;

    @ColumnInfo(name = "kisisayisi")
    public int kisiSayisi;

    @ColumnInfo(name = "malzeme")
    public String malzemeler;
}
