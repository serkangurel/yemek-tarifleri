package com.serkangurel.yemektarifleri.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import com.fstyle.library.helper.AssetSQLiteOpenHelperFactory;


@Database(entities = {RecipesEntity.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract RecipesDao recipesDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (instance == null) {
            //DB.copyAttachedDatabase(context, "recipes.db");
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class,
                    "recipes.db")
                    .openHelperFactory(new AssetSQLiteOpenHelperFactory())
                    .allowMainThreadQueries()
                    .addMigrations(MIGRATION_2_3)
                    .build();
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Create the new table
            database.execSQL("CREATE TABLE Yemektarifleri_new (id INTEGER NOT NULL, kategori TEXT, yemekad TEXT, resim TEXT, yemektarifi TEXT, hazirlamasuresi INTEGER NOT NULL DEFAULT 0, sure INTEGER NOT NULL DEFAULT 0, kisisayisi INTEGER NOT NULL DEFAULT 0, malzeme TEXT, PRIMARY KEY(id))");
            // Copy the data
            database.execSQL("INSERT INTO Yemektarifleri_new (id, kategori, yemekad, resim, yemektarifi, hazirlamasuresi, sure, kisisayisi, malzeme) SELECT id, kategori, yemekad, resim, yemektarifi, hazirlamasuresi, sure, kisisayisi, malzeme FROM Yemektarifleri");
            // Remove the old table
            database.execSQL("DROP TABLE Yemektarifleri");
            // Change the table name to the correct one
            database.execSQL("ALTER TABLE Yemektarifleri_new RENAME TO Yemektarifleri");

        }
    };


}