package com.serkangurel.yemektarifleri.utils;

import android.content.Context;
import android.os.Environment;

import com.serkangurel.yemektarifleri.BuildConfig;
import com.serkangurel.yemektarifleri.db.RecipesEntity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tools {

    private static final Tools ourInstance = new Tools();

    public static Tools getInstance() {
        return ourInstance;
    }

    private Tools() {
    }

    public String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String getDownloadPath() {
        return Environment.DIRECTORY_PICTURES + File.separator + getAppName();
    }

    public String getAppName() {
        String appId = BuildConfig.APPLICATION_ID;
        String[] myArray = appId.split("\\.");
        return myArray[myArray.length - 1];
    }

    public String firstLetterCap(String str) {
        String[] words = str.trim().split(" ");
        StringBuilder ret = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            if (words[i].trim().length() > 0) {
                if (i == 0) {
                    ret.append(Character.toUpperCase(words[i].trim().charAt(0)));
                    ret.append(words[i].trim().substring(1).toLowerCase());
                } else {
                    ret.append(words[i].trim().toLowerCase());
                }
                if (i < words.length - 1) {
                    ret.append(' ');
                }
            }
        }
        return ret.toString();
    }

    public List<String> getIngredientsList(RecipesEntity recipe) {
        String[] ingredientsArray = recipe.malzemeler.split("[\\r\\n]+"); // '\n' karakterine göre parçala
        return new ArrayList<>(Arrays.asList(ingredientsArray));
    }

}